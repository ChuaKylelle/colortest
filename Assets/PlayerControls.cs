// GENERATED AUTOMATICALLY FROM 'Assets/PlayerControls.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

public class @PlayerControls : IInputActionCollection, IDisposable
{
    public InputActionAsset asset { get; }
    public @PlayerControls()
    {
        asset = InputActionAsset.FromJson(@"{
    ""name"": ""PlayerControls"",
    ""maps"": [
        {
            ""name"": ""PlayerControl"",
            ""id"": ""7e508163-c401-4a6c-88b3-91966b9678f2"",
            ""actions"": [
                {
                    ""name"": ""FowardMovement"",
                    ""type"": ""PassThrough"",
                    ""id"": ""26f617f2-3792-409f-9930-23806c16a09e"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""SidewayMovement"",
                    ""type"": ""PassThrough"",
                    ""id"": ""f1f646ef-56d6-41c4-a204-7bd456b0705e"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Jump"",
                    ""type"": ""Button"",
                    ""id"": ""606fd0d5-52da-4297-8e82-4beee6d17bd9"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": ""Press""
                }
            ],
            ""bindings"": [
                {
                    ""name"": ""Forward"",
                    ""id"": ""5d5862d5-66c3-43fb-a580-430dc4b7f71f"",
                    ""path"": ""1DAxis"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""FowardMovement"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""negative"",
                    ""id"": ""9021c740-f1c3-40b6-b53c-c634f3b3d9d7"",
                    ""path"": ""<Keyboard>/s"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""FowardMovement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""positive"",
                    ""id"": ""618beb2a-7d09-40de-a5d4-0f6b5176b054"",
                    ""path"": ""<Keyboard>/w"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""FowardMovement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": """",
                    ""id"": ""5d3de4ff-1f11-4c9a-980c-0d7d7459b93d"",
                    ""path"": ""<Keyboard>/space"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Jump"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""Sideway"",
                    ""id"": ""c6db3cf0-d6d6-4681-b514-cf5ab775ab1b"",
                    ""path"": ""1DAxis"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""SidewayMovement"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""negative"",
                    ""id"": ""c1daa327-e28a-47f3-bc8e-f317c40e76db"",
                    ""path"": ""<Keyboard>/a"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""SidewayMovement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""positive"",
                    ""id"": ""7689079f-2f74-4265-a980-df9040b95c6f"",
                    ""path"": ""<Keyboard>/d"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""SidewayMovement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                }
            ]
        },
        {
            ""name"": ""PlayerInteraction"",
            ""id"": ""74c67188-2ab5-4e32-b2c7-c84ac6f24fb6"",
            ""actions"": [
                {
                    ""name"": ""Interact"",
                    ""type"": ""PassThrough"",
                    ""id"": ""861a6f7b-ee23-481f-8a96-86922d5c7b1a"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": ""Press""
                },
                {
                    ""name"": ""ExitPuzzle"",
                    ""type"": ""PassThrough"",
                    ""id"": ""d86c17cb-449e-48a4-9159-5348a2ea8a89"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": ""Press""
                },
                {
                    ""name"": ""TurnRayLight"",
                    ""type"": ""Button"",
                    ""id"": ""203c3290-6456-48ee-84c2-50b58b4b8c71"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": ""Press""
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""5131e2d1-c9a9-47df-9019-192216c300d4"",
                    ""path"": ""<Keyboard>/e"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Interact"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""632a1e41-da85-4071-9ad7-e132bc428514"",
                    ""path"": ""<Keyboard>/escape"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""ExitPuzzle"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""Horizontal"",
                    ""id"": ""a3589d63-4a2a-46af-ba63-b8ff31185726"",
                    ""path"": ""1DAxis"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""TurnRayLight"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""negative"",
                    ""id"": ""5118a960-f9c3-4dc8-8b41-13ff2f0807fa"",
                    ""path"": ""<Keyboard>/a"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""TurnRayLight"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""positive"",
                    ""id"": ""cb8694a1-0f6e-447f-9748-c1be5775d0dc"",
                    ""path"": ""<Keyboard>/d"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""TurnRayLight"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                }
            ]
        },
        {
            ""name"": ""Mouse"",
            ""id"": ""37726d8b-ebf8-4009-a6d7-ab090d7b7677"",
            ""actions"": [
                {
                    ""name"": ""Click"",
                    ""type"": ""Button"",
                    ""id"": ""9e9349c9-a49d-4439-83cd-5b4682d2efc6"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Position"",
                    ""type"": ""PassThrough"",
                    ""id"": ""15ce9962-0a5f-44ee-b72d-89b283e5abaf"",
                    ""expectedControlType"": ""Vector2"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""75330391-1a54-40de-b106-b7e51826d24d"",
                    ""path"": ""<Mouse>/leftButton"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Click"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""df715ac8-c1b5-4515-bfb7-1dcfe5f00969"",
                    ""path"": ""<Mouse>/position"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Position"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": []
}");
        // PlayerControl
        m_PlayerControl = asset.FindActionMap("PlayerControl", throwIfNotFound: true);
        m_PlayerControl_FowardMovement = m_PlayerControl.FindAction("FowardMovement", throwIfNotFound: true);
        m_PlayerControl_SidewayMovement = m_PlayerControl.FindAction("SidewayMovement", throwIfNotFound: true);
        m_PlayerControl_Jump = m_PlayerControl.FindAction("Jump", throwIfNotFound: true);
        // PlayerInteraction
        m_PlayerInteraction = asset.FindActionMap("PlayerInteraction", throwIfNotFound: true);
        m_PlayerInteraction_Interact = m_PlayerInteraction.FindAction("Interact", throwIfNotFound: true);
        m_PlayerInteraction_ExitPuzzle = m_PlayerInteraction.FindAction("ExitPuzzle", throwIfNotFound: true);
        m_PlayerInteraction_TurnRayLight = m_PlayerInteraction.FindAction("TurnRayLight", throwIfNotFound: true);
        // Mouse
        m_Mouse = asset.FindActionMap("Mouse", throwIfNotFound: true);
        m_Mouse_Click = m_Mouse.FindAction("Click", throwIfNotFound: true);
        m_Mouse_Position = m_Mouse.FindAction("Position", throwIfNotFound: true);
    }

    public void Dispose()
    {
        UnityEngine.Object.Destroy(asset);
    }

    public InputBinding? bindingMask
    {
        get => asset.bindingMask;
        set => asset.bindingMask = value;
    }

    public ReadOnlyArray<InputDevice>? devices
    {
        get => asset.devices;
        set => asset.devices = value;
    }

    public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

    public bool Contains(InputAction action)
    {
        return asset.Contains(action);
    }

    public IEnumerator<InputAction> GetEnumerator()
    {
        return asset.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public void Enable()
    {
        asset.Enable();
    }

    public void Disable()
    {
        asset.Disable();
    }

    // PlayerControl
    private readonly InputActionMap m_PlayerControl;
    private IPlayerControlActions m_PlayerControlActionsCallbackInterface;
    private readonly InputAction m_PlayerControl_FowardMovement;
    private readonly InputAction m_PlayerControl_SidewayMovement;
    private readonly InputAction m_PlayerControl_Jump;
    public struct PlayerControlActions
    {
        private @PlayerControls m_Wrapper;
        public PlayerControlActions(@PlayerControls wrapper) { m_Wrapper = wrapper; }
        public InputAction @FowardMovement => m_Wrapper.m_PlayerControl_FowardMovement;
        public InputAction @SidewayMovement => m_Wrapper.m_PlayerControl_SidewayMovement;
        public InputAction @Jump => m_Wrapper.m_PlayerControl_Jump;
        public InputActionMap Get() { return m_Wrapper.m_PlayerControl; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(PlayerControlActions set) { return set.Get(); }
        public void SetCallbacks(IPlayerControlActions instance)
        {
            if (m_Wrapper.m_PlayerControlActionsCallbackInterface != null)
            {
                @FowardMovement.started -= m_Wrapper.m_PlayerControlActionsCallbackInterface.OnFowardMovement;
                @FowardMovement.performed -= m_Wrapper.m_PlayerControlActionsCallbackInterface.OnFowardMovement;
                @FowardMovement.canceled -= m_Wrapper.m_PlayerControlActionsCallbackInterface.OnFowardMovement;
                @SidewayMovement.started -= m_Wrapper.m_PlayerControlActionsCallbackInterface.OnSidewayMovement;
                @SidewayMovement.performed -= m_Wrapper.m_PlayerControlActionsCallbackInterface.OnSidewayMovement;
                @SidewayMovement.canceled -= m_Wrapper.m_PlayerControlActionsCallbackInterface.OnSidewayMovement;
                @Jump.started -= m_Wrapper.m_PlayerControlActionsCallbackInterface.OnJump;
                @Jump.performed -= m_Wrapper.m_PlayerControlActionsCallbackInterface.OnJump;
                @Jump.canceled -= m_Wrapper.m_PlayerControlActionsCallbackInterface.OnJump;
            }
            m_Wrapper.m_PlayerControlActionsCallbackInterface = instance;
            if (instance != null)
            {
                @FowardMovement.started += instance.OnFowardMovement;
                @FowardMovement.performed += instance.OnFowardMovement;
                @FowardMovement.canceled += instance.OnFowardMovement;
                @SidewayMovement.started += instance.OnSidewayMovement;
                @SidewayMovement.performed += instance.OnSidewayMovement;
                @SidewayMovement.canceled += instance.OnSidewayMovement;
                @Jump.started += instance.OnJump;
                @Jump.performed += instance.OnJump;
                @Jump.canceled += instance.OnJump;
            }
        }
    }
    public PlayerControlActions @PlayerControl => new PlayerControlActions(this);

    // PlayerInteraction
    private readonly InputActionMap m_PlayerInteraction;
    private IPlayerInteractionActions m_PlayerInteractionActionsCallbackInterface;
    private readonly InputAction m_PlayerInteraction_Interact;
    private readonly InputAction m_PlayerInteraction_ExitPuzzle;
    private readonly InputAction m_PlayerInteraction_TurnRayLight;
    public struct PlayerInteractionActions
    {
        private @PlayerControls m_Wrapper;
        public PlayerInteractionActions(@PlayerControls wrapper) { m_Wrapper = wrapper; }
        public InputAction @Interact => m_Wrapper.m_PlayerInteraction_Interact;
        public InputAction @ExitPuzzle => m_Wrapper.m_PlayerInteraction_ExitPuzzle;
        public InputAction @TurnRayLight => m_Wrapper.m_PlayerInteraction_TurnRayLight;
        public InputActionMap Get() { return m_Wrapper.m_PlayerInteraction; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(PlayerInteractionActions set) { return set.Get(); }
        public void SetCallbacks(IPlayerInteractionActions instance)
        {
            if (m_Wrapper.m_PlayerInteractionActionsCallbackInterface != null)
            {
                @Interact.started -= m_Wrapper.m_PlayerInteractionActionsCallbackInterface.OnInteract;
                @Interact.performed -= m_Wrapper.m_PlayerInteractionActionsCallbackInterface.OnInteract;
                @Interact.canceled -= m_Wrapper.m_PlayerInteractionActionsCallbackInterface.OnInteract;
                @ExitPuzzle.started -= m_Wrapper.m_PlayerInteractionActionsCallbackInterface.OnExitPuzzle;
                @ExitPuzzle.performed -= m_Wrapper.m_PlayerInteractionActionsCallbackInterface.OnExitPuzzle;
                @ExitPuzzle.canceled -= m_Wrapper.m_PlayerInteractionActionsCallbackInterface.OnExitPuzzle;
                @TurnRayLight.started -= m_Wrapper.m_PlayerInteractionActionsCallbackInterface.OnTurnRayLight;
                @TurnRayLight.performed -= m_Wrapper.m_PlayerInteractionActionsCallbackInterface.OnTurnRayLight;
                @TurnRayLight.canceled -= m_Wrapper.m_PlayerInteractionActionsCallbackInterface.OnTurnRayLight;
            }
            m_Wrapper.m_PlayerInteractionActionsCallbackInterface = instance;
            if (instance != null)
            {
                @Interact.started += instance.OnInteract;
                @Interact.performed += instance.OnInteract;
                @Interact.canceled += instance.OnInteract;
                @ExitPuzzle.started += instance.OnExitPuzzle;
                @ExitPuzzle.performed += instance.OnExitPuzzle;
                @ExitPuzzle.canceled += instance.OnExitPuzzle;
                @TurnRayLight.started += instance.OnTurnRayLight;
                @TurnRayLight.performed += instance.OnTurnRayLight;
                @TurnRayLight.canceled += instance.OnTurnRayLight;
            }
        }
    }
    public PlayerInteractionActions @PlayerInteraction => new PlayerInteractionActions(this);

    // Mouse
    private readonly InputActionMap m_Mouse;
    private IMouseActions m_MouseActionsCallbackInterface;
    private readonly InputAction m_Mouse_Click;
    private readonly InputAction m_Mouse_Position;
    public struct MouseActions
    {
        private @PlayerControls m_Wrapper;
        public MouseActions(@PlayerControls wrapper) { m_Wrapper = wrapper; }
        public InputAction @Click => m_Wrapper.m_Mouse_Click;
        public InputAction @Position => m_Wrapper.m_Mouse_Position;
        public InputActionMap Get() { return m_Wrapper.m_Mouse; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(MouseActions set) { return set.Get(); }
        public void SetCallbacks(IMouseActions instance)
        {
            if (m_Wrapper.m_MouseActionsCallbackInterface != null)
            {
                @Click.started -= m_Wrapper.m_MouseActionsCallbackInterface.OnClick;
                @Click.performed -= m_Wrapper.m_MouseActionsCallbackInterface.OnClick;
                @Click.canceled -= m_Wrapper.m_MouseActionsCallbackInterface.OnClick;
                @Position.started -= m_Wrapper.m_MouseActionsCallbackInterface.OnPosition;
                @Position.performed -= m_Wrapper.m_MouseActionsCallbackInterface.OnPosition;
                @Position.canceled -= m_Wrapper.m_MouseActionsCallbackInterface.OnPosition;
            }
            m_Wrapper.m_MouseActionsCallbackInterface = instance;
            if (instance != null)
            {
                @Click.started += instance.OnClick;
                @Click.performed += instance.OnClick;
                @Click.canceled += instance.OnClick;
                @Position.started += instance.OnPosition;
                @Position.performed += instance.OnPosition;
                @Position.canceled += instance.OnPosition;
            }
        }
    }
    public MouseActions @Mouse => new MouseActions(this);
    public interface IPlayerControlActions
    {
        void OnFowardMovement(InputAction.CallbackContext context);
        void OnSidewayMovement(InputAction.CallbackContext context);
        void OnJump(InputAction.CallbackContext context);
    }
    public interface IPlayerInteractionActions
    {
        void OnInteract(InputAction.CallbackContext context);
        void OnExitPuzzle(InputAction.CallbackContext context);
        void OnTurnRayLight(InputAction.CallbackContext context);
    }
    public interface IMouseActions
    {
        void OnClick(InputAction.CallbackContext context);
        void OnPosition(InputAction.CallbackContext context);
    }
}
