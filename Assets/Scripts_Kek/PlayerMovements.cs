﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovements : MonoBehaviour
{
    [SerializeField] private bool canMove;
    public bool CanMove { get => canMove; set => canMove = value; }

    [SerializeField] private float moveSpeed;
    [SerializeField] private float jumpHeight;
    Vector3 forward;
    Vector3 right;
    bool isGrounded = true;

    private InputHandler inputHandler;

    private void Awake()
    {
        CanMove = true;
        forward = Camera.main.transform.forward;
        forward.y = 0;
        forward = Vector3.Normalize(forward);
        right = Quaternion.Euler(new Vector3(0, 90, 0)) * forward;

    }
    // Start is called before the first frame update
    void Start()
    {
        inputHandler = SingletonManager.Get<InputHandler>();
        inputHandler.GetPlayerControls().PlayerControl.Jump.performed += _ => Jump();
    }

    // Update is called once per frame
    void Update()
    {
        if (!canMove)
        {
            return;
        }
        float forwardInput = inputHandler.GetPlayerControls().PlayerControl.FowardMovement.ReadValue<float>();
        float rightInput = inputHandler.GetPlayerControls().PlayerControl.SidewayMovement.ReadValue<float>();

        Move(forwardInput, rightInput);

        
    }

    private void Move(float forwardInput, float rightInput)
    {
        Vector3 direction = new Vector3(rightInput, 0, forwardInput).normalized;
        Vector3 rightMovement = right * moveSpeed * Time.deltaTime * rightInput;
        Vector3 upMovement = forward * moveSpeed * Time.deltaTime * forwardInput;

        Vector3 heading = Vector3.Normalize(rightMovement + upMovement);
        transform.forward = heading;
        transform.position += rightMovement;
        transform.position += upMovement;


    }

    private void Jump()
    {
        Vector3 jumpVelocity = new Vector3(0f, jumpHeight, 0f);

        if (IsGrounded())
        {
            gameObject.GetComponent<Rigidbody>().velocity += jumpVelocity;
        }

    }

    private bool IsGrounded()
    {
        BoxCollider boxCollider = GetComponent<BoxCollider>();
        float extraHeightText = 0.01f;

        RaycastHit hit;
        Color rayColor;

        if (Physics.Raycast(boxCollider.bounds.center, Vector3.down, out hit, boxCollider.bounds.extents.y + extraHeightText))
        {
            if(hit.collider != null)
            {
                Debug.Log(hit.collider.name);
                rayColor = Color.green;
            }
            else
            {
                rayColor = Color.red;
            }
        }

        Debug.DrawRay(boxCollider.bounds.center, Vector3.down * (boxCollider.bounds.extents.y + extraHeightText));
        return hit.collider!= null;

    }
}
