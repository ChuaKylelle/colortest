﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectorObject : MonoBehaviour
{
    public int id;
    public LayerMask layer;

    bool isLinedUp = false;
    bool isActiveEnter = false;
    bool isActiveExit = false;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        DetectShadowLinedUp();

    }

    private void DetectShadowLinedUp()
    {
        RaycastHit hit;

        if (Physics.Raycast(gameObject.transform.position, gameObject.transform.forward, out hit, 100f, layer))
        {
            Debug.Log(hit.collider.name);
            isLinedUp = true;
            if (isLinedUp && !isActiveEnter)
            {
                isActiveExit = false;
                isActiveEnter = true;
                GameEvent.current.ProjectorObjectTriggerEnter(id);
            }

        }
        else
        {
            isLinedUp = false;
            if(!isLinedUp && !isActiveExit)
            {
                isActiveEnter = false;
                isActiveExit = true;
                GameEvent.current.ProjectObjecttriggerExit(id);
            }
        }

    }

}
