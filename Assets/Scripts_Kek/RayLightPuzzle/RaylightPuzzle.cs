﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using DG.Tweening;

[System.Serializable]
public class OnHitEvent : UnityEvent
{

}
[System.Serializable]
public class OnDisableEvent : UnityEvent
{

}

public class RaylightPuzzle : MonoBehaviour
{
    public OnHitEvent isHit;
    public OnDisableEvent isDisable;

    public float rotationSpeed;
    public Transform firePoint;
    public LayerMask reflectionLayer;
    public bool isActive = false;
        
    private Transform rayLightHead;
    private InteractionHandler interactionHandler;

    private bool canMove = false;
    
    public bool CanMove { get => canMove; set => canMove = value; }

    private void Awake()
    {
        rayLightHead = transform.Find("rayLightHead");
    }

    // Start is called before the first frame update
    void Start()
    {
        interactionHandler = SingletonManager.Get<InteractionHandler>();
    }

    // Update is called once per frame
    void Update()
    {
        if (canMove)
        {
            RotateRayPuzzle();
        }

        if (isActive)
        {
            RaycastHit hit;


            if (Physics.Raycast(firePoint.transform.position, firePoint.transform.forward, out hit, 10000f, reflectionLayer))
            {
                Debug.Log(hit.collider.name);
                hit.collider.gameObject.GetComponentInParent<RaylightPuzzle>().isHit?.Invoke();
              
            }
        }
       

    }

    private void RotateRayPuzzle()
    {
        float rotation = interactionHandler.GetPlayerControls().PlayerInteraction.TurnRayLight.ReadValue<float>();
        float rotate = rotation * rotationSpeed * Time.deltaTime;

        rayLightHead.Rotate(0, rotate, 0);
    }

    public void ActivateLightBeam()
    {
        isActive = true;
    }

    public void DeactivateLightBeam()
    {
        isActive = false;
    }





}
