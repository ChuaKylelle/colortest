﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInteract : MonoBehaviour
{
    [SerializeField] private bool canInteract;
    [SerializeField] GameObject puzzleObject;

    private Camera camera;
    private int puzzleId;


    public bool CanInteract { get => canInteract; set => canInteract = value; }

    private InteractionHandler interactionHandler;

    private void Awake()
    {
        camera = Camera.main;
    }

    // Start is called before the first frame update
    void Start()
    {
        interactionHandler = SingletonManager.Get<InteractionHandler>();
        interactionHandler.GetPlayerControls().PlayerInteraction.Interact.performed += _ => InteractPuzzle();
        interactionHandler.GetPlayerControls().PlayerInteraction.ExitPuzzle.performed += _ => ExitPuzzle();
        interactionHandler.GetPlayerControls().Mouse.Click.performed += _ => DetectPuzzle();
        GameEvent.current.onRayLightInteract += InteractRayLights;
        GameEvent.current.onRayLightExit += ExitRayLights;
        

    }

    // Update is called once per frame
    void Update()
    {
       
    }

    public void PuzzleDetected(GameObject puzzleObject)
    {
        this.puzzleObject = puzzleObject;
    }

    public void SetPuzzleToNull()
    {
        this.puzzleObject = null;
    }


    private void InteractPuzzle()
    {
        if (canInteract != false)
        {
            GetComponent<PlayerMovements>().CanMove = false;
            if (puzzleObject.GetComponent<PuzzleCamera>())
            {
                puzzleObject.GetComponent<PuzzleCamera>().GetPuzzleCamera().Priority = 11;
            }

            if (puzzleObject.GetComponent<RaylightPuzzle>())
            {
                var rayLightPuzzle = puzzleObject.GetComponent<RaylightPuzzle>();

                GameEvent.current.RayLightInteract();
            }
            
           
        }

    }

    private void ExitPuzzle()
    {
        if(canInteract != false)
        {
            GetComponent<PlayerMovements>().CanMove = true;
            if (puzzleObject.GetComponent<PuzzleCamera>())
            {
                puzzleObject.GetComponent<PuzzleCamera>().GetPuzzleCamera().Priority = 9;
            }

            if (puzzleObject.GetComponent<RaylightPuzzle>())
            {
                var rayLightPuzzle = puzzleObject.GetComponent<RaylightPuzzle>();

                GameEvent.current.RayLightExit();
            }

        }
    }

    private void DetectPuzzle()
    {
        if(!GetComponent<PlayerMovements>().CanMove)
        {
            Ray ray = camera.ScreenPointToRay(interactionHandler.GetPlayerControls().Mouse.Position.ReadValue<Vector2>());

            RaycastHit hit;
            if(Physics.Raycast(ray, out hit))
            {
                if(hit.collider.GetComponent<Block>())
                {
                    hit.collider.GetComponent<Block>().OnMouseDown();
                }
            }

        }
    }

    private void InteractRayLights()
    {
        if (!GetComponent<PlayerMovements>().CanMove)
        {
            puzzleObject.GetComponent<RaylightPuzzle>().CanMove = true;
        }
    }

    private void ExitRayLights()
    {
        puzzleObject.GetComponent<RaylightPuzzle>().CanMove = false;
    }
}
