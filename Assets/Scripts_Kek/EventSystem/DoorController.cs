﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorController : MonoBehaviour
{
    public int id;
    public bool isOpen;

    Vector3 originPosition;

    // Start is called before the first frame update
    void Start()
    {
        originPosition = transform.position;
        GameEvent.current.onProjectorObjectTriggerEnter += OnDoorwayOpen;
        GameEvent.current.onProjectorObjectTriggerExit += OnDoorwayClose;
        GameEvent.current.onPuzzleSlidingComplete += OnDoorwayOpen;
    }


    private void OnDoorwayOpen(int id)
    {
        if(id == this.id)
        {
            isOpen = true;
            LeanTween.moveLocalY(gameObject, originPosition.y + 3f, 1).setEaseOutQuad();
        }

    }

    private void OnDoorwayClose(int id)
    {

        if(id == this.id)
        {
            isOpen = false;
            LeanTween.moveLocalY(gameObject, originPosition.y, 1f).setEaseInQuad();
        }

    }

   

}
