﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameEvent : MonoBehaviour
{
    public static GameEvent current;

    private void Awake()
    {
        current = this;
    }

    public event Action<int> onProjectorObjectTriggerEnter;
    public void ProjectorObjectTriggerEnter(int id)
    {
        onProjectorObjectTriggerEnter?.Invoke(id);
    }

    public event Action<int> onProjectorObjectTriggerExit;
    public void ProjectObjecttriggerExit(int id)
    {
        onProjectorObjectTriggerExit?.Invoke(id);
    }

    public event Action<int> onPuzzleSlidingComplete;
    public void PuzzleSlidingComplete(int id)
    {
        onPuzzleSlidingComplete?.Invoke(id);
    }

    public event Action onRayLightInteract;
    public void RayLightInteract()
    {
        onRayLightInteract?.Invoke();
    }

    public event Action onRayLightExit;
    public void RayLightExit()
    {
        onRayLightExit?.Invoke();
    }
}
