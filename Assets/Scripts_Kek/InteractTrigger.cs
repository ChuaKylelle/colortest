﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class PuzzleDetected : UnityEvent
{
    
}

public class InteractTrigger : MonoBehaviour
{
    [SerializeField] private GameObject puzzle;
    public PuzzleDetected puzzleDetected;
    public PuzzleDetected puzzleExitTrigger;


    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<PlayerInteract>())
        {
            other.GetComponent<PlayerInteract>().CanInteract = true;
            puzzleDetected.Invoke();

        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.GetComponent<PlayerInteract>())
        {
            other.GetComponent<PlayerInteract>().CanInteract = false;
            puzzleExitTrigger.Invoke();
        }
    }
}
