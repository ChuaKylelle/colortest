﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class PuzzleCamera : MonoBehaviour
{
    [SerializeField] private CinemachineVirtualCamera puzzleCamera;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public CinemachineVirtualCamera GetPuzzleCamera()
    {
        return puzzleCamera;
    }
}
